
#include <ros.h>
// #include <std_msgs/String.h>
// #include <std_msgs/Float64.h>
#include <ma2000_msg/ma2000Encoder.h>

#define encoder_m1 A8     
#define encoder_m2 A9
#define encoder_m3 A10    // set input from each encoder of robot 

// ros::NodeHandle nh;
// std_msgs::String str_msg;
// ros::Publisher chatter("", &str_msg);
// char hello[13] = "hello world!";

// init node 
ros::NodeHandle nh;
ma2000_msg::ma2000Encoder encoder;
ros::Publisher pubEncoder("ma2000Encoder", &encoder);

int en_m1, en_m2, en_m3;

void setup()
{
	pinMode(encoder_m1, INPUT);
	pinMode(encoder_m2, INPUT);
	pinMode(encoder_m3, INPUT);


	nh.initNode();
 	nh.advertise(pubEncoder);
	
}

void loop()
{
	en_m1 = analogRead(encoder_m1);
	en_m2 = analogRead(encoder_m2);
	en_m3 = analogRead(encoder_m3);
	encoder.encoderM1 = en_m1;
	encoder.encoderM2 = en_m2;
	encoder.encoderM3 = en_m3;
	pubEncoder.publish(&encoder);
	nh.spinOnce();
	
}
