// PID motor position control.
// Thanks to Brett Beauregard for his nice PID library http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/

#include <PinChangeInt.h>
#include <PID_v1.h>
#include <math.h>

#define encoder_m1 A8     
#define encoder_m2 A9
#define encoder_m3 A10    // set input from each encoder of robot 

int RPWM1=2;
int LPWM1=3;
int RPWM2=4;
int LPWM2=5;
int RPWM3=6;
int LPWM3=7;  // set PWM M1-M3

int R_EN1=22;
int L_EN1=23;
int R_EN2=24;
int L_EN2=25;
int R_EN3=26;
int L_EN3=27; // 

int i=200,c=0;

double L1 = 230.0; //mm
double L2 = 250.0; //mm
double Pi_const=3.14159265358979323846;


double Px = 0;
double Py = 0;
double Pz = 0; // start 0 0 480 mm  intput for P-control X Y Z

int PPWM = 50, MPWM = -50;

//double te1,te2,te3; // ???

double kp1 = 12.48 , ki1 = 0.15 , kd1 = 0;             
double kp2 = 21.28 , ki2 = 1.57 , kd2 = 0.02;          
double kp3 = 14.1 , ki3 = 0.19 , kd3 = 0;              //for Drive M1 M2 M3   

double kpx = 0.05 , kix = 0 , kdx = 0;           
double kpy = 0.05 , kiy = 0 , kdy = 0;          
double kpz = 0.05 , kiz = 0 , kdz = 0;  // for PID X Y Z          

double input1 = 0, output1 = 0, setpoint1 = 0;
double input2 = 0, output2 = 0, setpoint2 = 0;
double input3 = 0, output3 = 0, setpoint3 = 0; // set input output setpoint M1 M2 M3

double inputx = 0, outputx = 0, setpointx = 0;
double inputy = 0, outputy = 0, setpointy = 0;
double inputz = 0, outputz = 0, setpointz = 0; // set input output setpoint PID X Y Z

double d1,d2,d3;
double xf,yf,zf,cf;    // for forward

double p1,p2,p3; // output from P-control X Y Z

double theta1, theta2, theta3;   /// for inverse             

double potenx, poteny, potenz;

long temp;
unsigned long time;  //time

PID myPID1(&input1, &output1, &setpoint1, kp1, ki1, kd1, DIRECT);  // for M1
PID myPID2(&input2, &output2, &setpoint2, kp2, ki2, kd2, DIRECT);  // for M2
PID myPID3(&input3, &output3, &setpoint3, kp3, ki3, kd3, DIRECT);  // for M3

PID myPIDx(&inputx, &outputx, &setpointx, kpx, kix, kdx, DIRECT);  // for X
PID myPIDy(&inputy, &outputy, &setpointy, kpy, kiy, kdy, DIRECT);  // for Y
PID myPIDz(&inputz, &outputz, &setpointz, kpz, kiz, kdz, DIRECT);  // for Z

void setup() {
  
  Serial.begin(9600);

  TCCR1B = TCCR1B & 0b11111000 | 1;                   // set 31KHz PWM to prevent motor noise
  
  myPIDx.SetMode(AUTOMATIC);
  myPIDx.SetSampleTime(1);
  myPIDx.SetOutputLimits(-10, 10);
  
  myPIDy.SetMode(AUTOMATIC);
  myPIDy.SetSampleTime(1);
  myPIDy.SetOutputLimits(-10, 10);
  
  myPIDz.SetMode(AUTOMATIC);
  myPIDz.SetSampleTime(1);
  myPIDz.SetOutputLimits(-10, 10);
  
  myPID1.SetMode(AUTOMATIC);
  myPID1.SetSampleTime(1);
  myPID1.SetOutputLimits(MPWM, PPWM);
  
  myPID2.SetMode(AUTOMATIC);
  myPID2.SetSampleTime(1);
  myPID2.SetOutputLimits(MPWM, PPWM);
  
  myPID3.SetMode(AUTOMATIC);
  myPID3.SetSampleTime(1);
  myPID3.SetOutputLimits(MPWM, PPWM);

  for(int i=2;i<=7;i++){
    pinMode(i,OUTPUT);
  }
  for(int i=22;i<=27;i++){
    pinMode(i,OUTPUT);
  }
  pinMode(A8,INPUT);
  pinMode(A9,INPUT);
  pinMode(A10,INPUT);
  
  for(int i=2;i<=7;i++){
   analogWrite(i,0);
  }
  for(int i=22;i<=27;i++){
   digitalWrite(i,HIGH);
  } 
//  Px=xf;
}

void loop() {
  inputxyz();
  forward_3DOF();
//  positionPIDxyz();
  inverse_3DOF(Px,Py,Pz);
  check_and_drive();                                     

  Serial.println(" ");
}

void check_and_drive(){
   
  if ( (theta1 < 135 )&&(theta1 > -135) ) positionM1(theta1);                                     
  else Serial.print("Over M-1");
 
  if ( (theta2 < 135 )&&(theta2 > -135) ) positionM2(theta2);                                     
  else Serial.print("Over M-2");
  
  if ( (theta3 < 135 )&&(theta3 > -135) ) positionM3(theta3); 
  else Serial.print("Over M-3");
}
void inverse_3DOF(double x,double y,double z){
  theta1 = atan2(y,x);//atan2(double y,double x)
  double c = sqrt(pow(x,2)+pow(y,2));
  theta3 = acos((c*c + z*z - L1*L1 - L2*L2)/(2*L1*L2));
  theta2 = atan2(c,z) - acos((c*c + z*z + L1*L1 - L2*L2)/(2*L1*sqrt(c*c + z*z)));
  theta1 = (180*theta1)/Pi_const;
  theta2 = (180*theta2)/Pi_const;
  theta3 = (180*theta3)/Pi_const;
  Serial.print("inverse");
  Serial.print("\t");
  Serial.print(theta1);
  Serial.print("\t");
  Serial.print(theta2);
  Serial.print("\t");
  Serial.print(theta3);
  Serial.print("\t");  
}

void positionM1(int Point1) {                                // to H-Bridge board
  digitalWrite(R_EN1,HIGH);
  digitalWrite(L_EN1,HIGH);
  setpoint1 = map(Point1,180,-180,0,1023);                      // modify to fit motor and encoder characteristics, potmeter connected to A0
  input1 = analogRead(8) ;                                // data from encoder
  myPID1.Compute();                                    // calculate new output

  if (input1 > (setpoint1-3) && input1 < setpoint1+3) output1 = 0;
  if (output1 > PPWM*0.01 && output1 < MPWM*0.01)     output1 = 0;
  
  if (output1 > 0) {
    analogWrite(RPWM1, abs(output1));                             // drive motor CW
    analogWrite(LPWM1, 0);
    
  }
  else {
    analogWrite(RPWM1, 0);
    analogWrite(LPWM1, abs(output1));
  }
}

void positionM2(int Point2) {                                // to H-Bridge board
  digitalWrite(R_EN2,HIGH);
  digitalWrite(L_EN2,HIGH);
  setpoint2 = map(Point2,-180,180,0,1023);                      // modify to fit motor and encoder characteristics, potmeter connected to A0
  input2 = analogRead(9) ;                                // data from encoder
  myPID2.Compute();// calculate new output
  
  if (input2 > (setpoint2-3) && input2 < setpoint2+3) output2 = 0;
  if (output2 > PPWM*0.01 && output2 < MPWM*0.01)     output2 = 0;
  
  if (output2 > 0) {
    analogWrite(RPWM2, abs(output2));                             // drive motor CW
    analogWrite(LPWM2, 0);
  }
  else {
    analogWrite(RPWM2, 0);
    analogWrite(LPWM2, abs(output2));
  }
}

void positionM3(int Point3) {                                // to H-Bridge board
  digitalWrite(R_EN3,HIGH);
  digitalWrite(L_EN3,HIGH);
  setpoint3 = map(Point3,-180,180,1023,0);                      // modify to fit motor and encoder characteristics, potmeter connected to A0
  input3 = analogRead(10) ;                                // data from encoder
  myPID3.Compute();  

  if (input3 > (setpoint3-3) && input3 < setpoint3+3) output3 = 0;
  if (output3 > PPWM*0.01 && output3 < MPWM*0.01)     output3 = 0;
  
  if (output3 > 0) {
    analogWrite(RPWM3, abs(output3));                             // drive motor CW
    analogWrite(LPWM3, 0);
    
  }
  else {
    analogWrite(RPWM3, 0);
    analogWrite(LPWM3, abs(output3));
  }
}

void forward_3DOF(){
    
  d1 = map(analogRead(8),0,1023,180,-180);
  d2 = map(analogRead(9),0,1023,-180,180);
  d3 = map(analogRead(10),1023,0,-180,180);

  d1 = d1*Pi_const/180;
  d2 = d2*Pi_const/180;
  d3 = d3*Pi_const/180;

  zf = (L1*cos(d2))+(cos(d2+d3)*L2);
  cf = (L2*sin(d2))+(sin(d2+d3)*L2);
  xf = cf*cos(d1);
  yf = cf*sin(d2);

  Serial.print("Forward");
  Serial.print("\t");
  Serial.print(xf);
  Serial.print("\t");
  Serial.print(yf);
  Serial.print("\t");
  Serial.print(zf);
  Serial.print("\t"); 
}

void inputxyz(){

//  if (xf<=205) c=0;
//  else if (xf>=395) c=1;
//    
//  if (c==0) i=i+2;
//  else if (c==1) i=i-2;
//  if (i>=400) i=400;
//  else if (i<=200) i=200;
//
//  if (xf<=215) i=400;
//  if (xf>=395) i=200;
  
  Px=0;
  Py=0;
  Pz=480;

//  if (xf==200) Px=400;
//  if (xf==400) Px=200;

  
//  Serial.print("XYZ Ref");
//  Serial.print("\t");
//  Serial.print(Px);
//  Serial.print("\t");
//  Serial.print(Py);
//  Serial.print("\t");
//  Serial.print(Pz);
//  Serial.print("\t");
//  Serial.print(Pz);
//  Serial.print("\t");
//  Serial.print(c);
}

