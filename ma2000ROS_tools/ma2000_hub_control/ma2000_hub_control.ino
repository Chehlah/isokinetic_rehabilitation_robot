/*
 * Aufa 14/11/2018 
 * 
 * Test Re-overall close loop 
 * ROS PID pkg
 */

#include <ros.h>
#include <ma2000_msg/ma2000pwmMotor.h>
#include <std_msgs/Float64.h>
#include <Timer.h>
#include <Filters.h>

#define encoder_m1 A1     
#define encoder_m2 A2
#define encoder_m3 A3    // set input from each encoder of robot 

double en_m1, en_m2, en_m3;

// set Pin PWM M1-M3
int RPWM1=2;
int LPWM1=3;
int RPWM2=4;
int LPWM2=5;
int RPWM3=6;
int LPWM3=7;  

// set Pin turn left and right
int R_EN1=22;
int L_EN1=23;
int R_EN2=24;
int L_EN2=25;
int R_EN3=26;
int L_EN3=27; 

// Param output PWM
int Rpwm1 = 0;
int Lpwm1 = 0;
int Rpwm2 = 0;
int Lpwm2 = 0;
int Rpwm3 = 0;
int Lpwm3 = 0;

ros::NodeHandle nh;

std_msgs::Float64 encoder1;
std_msgs::Float64 encoder2;
std_msgs::Float64 encoder3;

ros::Publisher pubEncoder1("/encoder1/state", &encoder1);
ros::Publisher pubEncoder2("/encoder2/state", &encoder2);
ros::Publisher pubEncoder3("/encoder3/state", &encoder3);

Timer tx;

float filterFrequency = 100.0; // 1:1  Hz
//float filterFrequency2 = 100.0; // 10 Hz
FilterOnePole lowpassFilterEncoder1( LOWPASS, filterFrequency );
FilterOnePole lowpassFilterEncoder2( LOWPASS, filterFrequency );
FilterOnePole lowpassFilterEncoder3( LOWPASS, filterFrequency );

void callBack( const ma2000_msg::ma2000pwmMotor& pwmMotor){
   Rpwm1 = pwmMotor.Rpwm1;
   Lpwm1 = pwmMotor.Lpwm1;
   Rpwm2 = pwmMotor.Rpwm2;
   Lpwm2 = pwmMotor.Lpwm2;
   Rpwm3 = pwmMotor.Rpwm3;
   Lpwm3 = pwmMotor.Lpwm3;
    analogWrite(RPWM1, Rpwm1); 
    analogWrite(LPWM1, Lpwm1);
    analogWrite(RPWM2, Rpwm2); 
    analogWrite(LPWM2, Lpwm2);
    analogWrite(RPWM3, Rpwm3); 
    analogWrite(LPWM3, Lpwm3);     
}
ros::Subscriber<ma2000_msg::ma2000pwmMotor> subPWM("ma2000pwmMotor", &callBack );

void setup() {
  // put your setup code here, to run once:
  pinMode(encoder_m1, INPUT);
  pinMode(encoder_m2, INPUT);
  pinMode(encoder_m3, INPUT);

  for(int i=2;i<=7;i++) pinMode(i,OUTPUT);
  for(int i=22;i<=27;i++) pinMode(i,OUTPUT);

  for(int i=2;i<=7;i++) analogWrite(i,0);
  for(int i=22;i<=27;i++) digitalWrite(i,HIGH);

  nh.initNode();
  nh.advertise(pubEncoder1);
  nh.advertise(pubEncoder2);
  nh.advertise(pubEncoder3);
   nh.subscribe(subPWM);

  int tickEvent = tx.every(10, encoderRead); // 100Hz
}

void loop() {//400Hz++
  // put your main code here, to run repeatedly:
  tx.update();
  lowpassFilterEncoder1.input( analogRead(encoder_m1) );
  lowpassFilterEncoder2.input( analogRead(encoder_m2) );
  lowpassFilterEncoder3.input( analogRead(encoder_m3) );

  nh.spinOnce();
}

void encoderRead(){
  en_m1 = lowpassFilterEncoder1.output();
  en_m2 = lowpassFilterEncoder2.output();
  en_m3 = lowpassFilterEncoder3.output();
  encoder1.data = en_m1;
  encoder2.data = en_m2;
  encoder3.data = en_m3;
  pubEncoder1.publish(&encoder1);
  pubEncoder2.publish(&encoder2);
  pubEncoder3.publish(&encoder3); 

}
